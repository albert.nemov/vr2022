﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeTrigger : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        Debug.Log($"Objet enter. Name object : {other.name}");
    }

    private void OnCollisionEnter(Collision collision)
    {
        Debug.Log("Collision enter.");
    }
}
