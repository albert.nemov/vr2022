﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreController : MonoBehaviour
{
    private int score;
    public Text textScore;

    private void Start()
    {
        score = 0;
    }

    public void UpScore()
    {
        score++;
        textScore.text = $"Score: {score}/4";
    }
}
