using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class Enemy : MonoBehaviour
{
    public List<Transform> points;
    public float speedWalk;
    public float speedRun;

    public float distanceDamage;
    public float distanceVisibility;
    public Transform player;

    private int indexCurPoint = 0;
    public Animator animator;
    public float timeReloadAttack;
    private bool isReload = false;

    private FPSController _fpsController;


    private int Hp;

    private void Start()
    {
        _fpsController = player.GetComponent<FPSController>();
        Hp = 100;
    }

    private void Update()
    {
        if (Hp <= 0)
            return;
        
        float distanceEnemyToPlayer = Vector3.Distance(transform.position, player.position);

        if (distanceEnemyToPlayer < distanceDamage)
        {
            animator.SetBool("Walk Forward", false);
            animator.SetBool("Run Forward", false);
            if (!isReload)
            {
                animator.SetTrigger("Attack 01");
                StartCoroutine(Reload());
            }
        }
        else if (distanceEnemyToPlayer < distanceVisibility)
        {
            animator.SetBool("Walk Forward", false);
            animator.SetBool("Run Forward", true);
            Vector3 newPlayerPos = new Vector3(player.position.x, transform.position.y, player.position.z);
            transform.LookAt(newPlayerPos);
            transform.Translate(Vector3.forward * speedRun * Time.deltaTime);
        }
        else
        {
            animator.SetBool("Run Forward", false);
            animator.SetBool("Walk Forward", true);
            Vector3 newPosPoint = new Vector3(points[indexCurPoint].position.x, transform.position.y, points[indexCurPoint].position.z);
            transform.LookAt(newPosPoint);
            transform.Translate(Vector3.forward * speedWalk * Time.deltaTime);

            if (Vector3.Distance(transform.position, newPosPoint) < 3)
            {
                indexCurPoint = (indexCurPoint + 1) % points.Count;
            }
        }
    }


    public IEnumerator Reload()
    {
        isReload = true;
        yield return new WaitForSeconds(0.5f);
        _fpsController.Damage(20);
        yield return new WaitForSeconds(timeReloadAttack);
        isReload = false;
    }

    public void Damage(int hp)
    {
        animator.SetTrigger("Take Damage");

        if (Hp > 0 && Hp - hp <= 0)
        {
            animator.SetBool("Walk Forward", false);
            animator.SetBool("Run Forward", false);
            animator.SetTrigger("Die");
            Destroy(gameObject, 5);
        }
        Hp -= hp;
    }
}
