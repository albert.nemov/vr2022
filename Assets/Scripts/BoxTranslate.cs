﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxTranslate : MonoBehaviour
{
    public float speed;
    public int direction = 1;

    private bool isDead = false;
    public ScoreController scoreController;

    // Update is called once per frame
    private void Update()
    {
        if (!isDead)
        {
            if (transform.position.x >= 8)
            {
                direction = -1;
            }
            else if (transform.position.x <= -8)
            {
                direction = 1;
            }

            transform.position = new Vector3(transform.position.x + Time.deltaTime * speed * direction,
                transform.position.y, transform.position.z);
        }
        else
        {
            GetComponent<Rigidbody>().useGravity = true;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.tag == "Ball")
        {
            if (!isDead)
            {
                isDead = true;
                scoreController.UpScore();
            }
        }
    }
}
