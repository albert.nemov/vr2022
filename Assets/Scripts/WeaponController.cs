﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponController : MonoBehaviour
{
    public GameObject ball;
    public Transform endPoint;
    public float forceShoot;

    public Transform parentPool;
    private Transform[] _pool;
    public int poolSize = 3;
    private int _curElPool = 0;

    private void Start()
    {
        _pool = new Transform[poolSize];
        for (int i = 0; i < poolSize; i++)
        {
            var newBall = Instantiate(ball, parentPool);
            newBall.SetActive(false);
            _pool[i] = newBall.transform;
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Shot();
        }

        float vertical = Input.GetAxis("Vertical"); // [-1; 1]
        float horizontal = Input.GetAxis("Horizontal"); // [-1; 1]
        transform.Rotate(new Vector3(-vertical, 0, -horizontal));
    }

    public void Shot()
    {
        var newBall = _pool[_curElPool];
        newBall.transform.position = endPoint.position;
        newBall.gameObject.SetActive(true);
        _curElPool++;
        if (_curElPool >= poolSize)
        {
            _curElPool = 0;
        }
        Rigidbody rigidbody = newBall.GetComponent<Rigidbody>();
        rigidbody.velocity = Vector3.zero;
        rigidbody.AddForce(transform.up * forceShoot, ForceMode.Impulse);
        StartCoroutine(TimeBall(newBall.gameObject));
    }

    private IEnumerator TimeBall(GameObject curBall)
    {
        yield return new WaitForSeconds(2);
        curBall.SetActive(false);
    }
}
