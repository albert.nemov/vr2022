using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    public void StartShootingScene()
    {
        SceneManager.LoadScene("FPS");
    }
    
    public void StartDemoScene()
    {
        SceneManager.LoadScene("SampleScene");
    }

    public void ExitToMenu()
    {
        SceneManager.LoadScene("MenuScene");
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            ExitToMenu();
        }
    }
}
