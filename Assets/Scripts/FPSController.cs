﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FPSController : MonoBehaviour
{
    public float Speed;
    public float Sensitivity;
    private CharacterController _characterController;
    private Camera _camera;

    private int Hp;

    private float _rotationX = 0;

    public Image ImageHp;

    public Menu Menu;

    private float moveHight = 0;

    private void Start()
    {
        _characterController = GetComponent<CharacterController>();
        _camera = Camera.main;
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        Hp = 100;
        UpdateImageHp();
    }

    private void Update()
    {
        Vector3 vertical = transform.forward * Input.GetAxis("Vertical") * Speed;
        Vector3 horizontal = transform.right * Input.GetAxis("Horizontal") * Speed;

        float mouseY = Input.GetAxis("Mouse Y");
        float mouseX = Input.GetAxis("Mouse X");
        
        float groundedPower = _characterController.isGrounded ? 0 : 9;

        moveHight -= groundedPower * Time.deltaTime;

        if (Input.GetKeyDown(KeyCode.Space) && _characterController.isGrounded)
        {
            moveHight = 5;
        }

        Vector3 dir = vertical + horizontal;
        dir.y = moveHight;

        Vector3 motion = dir * Time.deltaTime;

        _characterController.Move(motion);

        _rotationX -= mouseY * Sensitivity;
        _rotationX = Mathf.Clamp(_rotationX, -50, 50);
        
        _camera.transform.localRotation = Quaternion.Euler(_rotationX, 0, 0);
        transform.rotation *= Quaternion.Euler(0, mouseX * Sensitivity, 0);
    }

    public void Damage(int damage)
    {
        Hp -= damage;
        UpdateImageHp();

        if (Hp <= 0)
        {
            Menu.ExitToMenu();
        }
    }

    public void Repair(int hp)
    {
        Hp += hp;
        UpdateImageHp();
    }

    public void UpdateImageHp()
    {
        ImageHp.fillAmount = (float)Hp / 100;
    }
}
