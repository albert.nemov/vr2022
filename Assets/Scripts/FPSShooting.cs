using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FPSShooting : MonoBehaviour
{
    private Camera _camera;
    public LineRenderer lineRenderer;
    public Transform endPointWeapon;

    public float forceShoot = 2;

    private void Start()
    {
        lineRenderer.positionCount = 2;
        _camera = Camera.main;
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit; // хранит информацию объекта куда попоал луч

            var fromPos = _camera.ViewportToWorldPoint(new Vector3(Screen.width / 2, Screen.height / 2, 0));

            if (Physics.Raycast(fromPos, _camera.transform.forward, out hit))
            {
                lineRenderer.SetPosition(0, endPointWeapon.position);
                lineRenderer.SetPosition(1, hit.point);
                StartCoroutine(ShowLine());
               
                
                if (hit.transform.CompareTag("Target"))
                {
                    hit.transform.GetComponent<Rigidbody>().AddForce(-hit.normal * forceShoot, ForceMode.Impulse);
                }

                if (hit.transform.CompareTag("Enemy"))
                {
                    hit.transform.GetComponent<Enemy>().Damage(20);
                }
            }
        }
    }

    private IEnumerator ShowLine()
    {
        lineRenderer.enabled = true;

        yield return new WaitForSeconds(0.05f);

        lineRenderer.enabled = false;
    }
}
